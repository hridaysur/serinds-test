# Serind's Lab Front-End Test

React App to search Movie's.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

For development, you will only need Node.js and a node global package(NPM) installed in your environement.

```
### Node
- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Ubuntu

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm

- #### Other Operating Systems
  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

If the installation was successful, you should be able to run the following command.

    $ node --version
    v8.11.3

    $ npm --version
    6.1.0

If you need to update `npm`, you can make it using `npm`! Cool right? After running the following command, just open again the command line and be happy.

    $ npm install npm -g
```

### Installing

A step by step series of examples that tell you how to get a development env running

```
git clone https://hridaysur@bitbucket.org/hridaysur/serinds-test.git
cd serinds-test
npm install
```

## Running the Project

```
npm run start
```
This command should start this component on localhost:8080

## Built With

* [ReactJS](https://reactjs.org/docs/getting-started.html) - The React framework used for Front-End
* [OMDbAPI](http://www.omdbapi.com/) - 3rd party API to get the Movie
