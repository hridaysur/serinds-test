let APIUrl = `http://www.omdbapi.com/?apikey=fa3f8a2d`;

const getMovies = async (title, type = null, year =  null) => {
    const movies = [];
    try {
        const movies = await getMoviesFromApi(title, type, year);
        if(movies.Response !== 'True') {
            return wrapErrorResponse(movies.Error);
        } 
        return wrapSuccessfulResponse(movies);
    } catch (error) {
        return wrapErrorResponse(error);
    }
}

const getMoviesFromApi = async (title, type, year) => {
    const url = new URL(APIUrl);
    url.searchParams.append('s', title);
    if(type !== null) { url.searchParams.append('type', type) };
    if(year !== null) { url.searchParams.append('y', year) };
    try {
        const response = await fetch(url).then(result => result.json());
        return response;
    } catch (error) {
        return error;
    }
}

const getMovieByID = async (id) => {
    try {
        const movieDetail = await getMovieByIDFromApi(id);
        return wrapSuccessfulResponse(movieDetail);
    } catch (error) {
        return wrapErrorResponse(error);
    }
}

const getMovieByIDFromApi = async (id) => {
    const url = new URL(APIUrl);
    url.searchParams.append('i',id);
    try {
        const response = await fetch(url).then(result => result.json());
        return response;
    } catch (error) {
        return error;
    }
}

const getMovieModel = (movies) => {
    if (movies.length !== 0) {
        return movies.Search.map( (movie, index) => {
            return {
                key: index,
                title: movie.Title,
                image: movie.Poster,
                year: movie.Year,
                type: movie.Type,
                imdbID: movie.imdbID
            }
        })
    }
    return [];
}

const getMovieDetailModel = (movie) => {
    return {
        title: movie.Title,
        image: movie.Poster,
        year: movie.Year,
        type: movie.Type,
        released: movie.Released,
        genre: movie.Genre,
        ratings: movie.Ratings
    }
}

function wrapSuccessfulResponse(result) {
    return {
        success: true,
        result: result
    }
}

function wrapErrorResponse(response) {
    return {
        success: false,
        status: (response ? response : "Error Fetching Data")
    }
}


export default { 
    getMovies: getMovies,
    getMovieModel: getMovieModel,
    getMovieByID: getMovieByID,
    getMovieDetailModel: getMovieDetailModel
};