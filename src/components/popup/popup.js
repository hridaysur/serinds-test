import React from 'react';
import './popup.scss';
import MovieRating from './movieRating.js';

const popup = (props) => {

    const ratings = ( props.ratings.map((r,index) => {
        return <MovieRating key={index} value={r.Value} source={r.Source}/>
    }));
    
    return ( 
        <div className="popup"> 
        <div className="wrapper">
            <div className="profile-card js-profile-card">
                <div className="profile-card__img">
                    <img src={props.image} alt="profile card"></img>
                </div>
        
                <div className="profile-card__cnt js-profile-cnt">
                    <div className="profile-card__name">{props.title}</div>
                    <div className="profile-card__txt">Type: <strong>{props.type}</strong></div>
                    <div className="profile-card__txt"><strong>{props.genre}</strong></div>
                    <div className="profile-card-loc">
                        <span className="profile-card-loc__txt">
                           Released On: {props.released}
                        </span>
                    </div>
            
                    <div className="profile-card-inf">
                        {ratings}
                    </div>
                    <a onClick={props.closePopup} className="close"></a>
                </div>
            </div>
        </div>
        </div>
    );  
}

export default popup;
