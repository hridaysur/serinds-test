import React from 'react';

const movieRating = (props) => {
    return (
        <div className="profile-card-inf__item">
            <div className="profile-card-inf__title">{props.value}</div>
            <div className="profile-card-inf__txt">{props.source}</div>
        </div>
    )
}

export default movieRating;