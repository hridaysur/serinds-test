import React from "react";
import './movie.scss';


export default class movie extends React.Component {

    constructor() {
        super();
        this.handleDetailsClick = this.handleDetailsClick.bind(this);
    }

    handleDetailsClick(event) {
        event.persist();
        this.props.handleOnDetailsClick(event.target.value);
    }

    render() {
        return (
            <div className="col-lg-2 card mb-4">
                <img className="card-img-top" src={ this.props.image } alt="movie logo"/>
                <div className="card-body">
                    <h5 className="card-title">{ this.props.title }</h5>
                </div>
                <ul className="list-group list-group-flush">
                <li className="list-group-item">Year : { this.props.year }</li>
                <li className="list-group-item">Type : { this.props.type }</li>
            </ul>
            <div className="card-body">
                <button className="btn btn-primary" value={ this.props.id } onClick={this.handleDetailsClick} >Details</button>
            </div>
            </div>
        )
    }
};

