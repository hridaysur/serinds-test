import React from 'react';
import './LoadingSpinner.scss';

const loadingSpinner = () => (
  
  <div>
  <i className="fa fa-spinner fa-spin big-icon" /> 
  <p className="bold-font">Loading...</p>
  </div>


);

export default loadingSpinner;