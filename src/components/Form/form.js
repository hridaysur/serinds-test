import React from 'react';
import TextInput from './textBox';
import SelectInput from './selectInput';
import Button from './button';

export default class Form extends React.Component {
    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        const data = {
            title : event.target[1].value,
            type : event.target[3].value ? event.target[3].value : null,
            year : event.target[5].value ? event.target[5].value : null
        }
        this.props.handelOnSubmit(data);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <TextInput
                    hasLabel='true'
                    htmlFor='Title'
                    label='Title'
                    name='title'
                    required
                    type='text'
                />

                <SelectInput
                    hasLabel='true'
                    htmlFor='type'
                    label='Type'
                    name='type'
                    options={this.props.typeOptions}
                />

                <TextInput
                    hasLabel='true'
                    htmlFor='year'
                    label='Year'
                    name='year'
                    type='text'
                />
                
                <Button
                    type='submit'
                    value='submit'
                    text='Search'
                />
            </form>
        );
    }
}

