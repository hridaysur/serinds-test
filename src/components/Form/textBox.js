import React from 'react';
import Label from './label';

const textInput = (props) => (
    <fieldset>
          <Label
            hasLabel={props.hasLabel}
            htmlFor={props.htmlFor}
            label={props.label}
          />
  
          <input
            id={props.htmlFor}
            name={props.name || null}
            placeholder={props.placeholder || null}
            required={props.required || null}
            type={props.type || 'text'}
          />
      </fieldset>
);

export default textInput;