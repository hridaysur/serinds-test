import React from 'react';

const Button = (props) => (
    <fieldset>
        <button
          type={props.type || 'button'}
          value={props.value || null}
        >
          {props.text}
        </button>
    </fieldset>
)

export default Button;