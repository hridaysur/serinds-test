import React from 'react';
import Label from './label';

const selectInput = (props) => {
    // Get all options from option prop
    const selectOptions = props.options;

    // Generate list of options
    const selectOptionsList = selectOptions.map((selectOption, index) => {
      return <option key={index} value={index}>{selectOption}</option>
    });

    return <fieldset>
        <Label
          hasLabel={props.hasLabel}
          htmlFor={props.htmlFor}
          label={props.label}
        />
        
        <select
          defaultValue=''
          id={props.htmlFor}
          name={props.name || null}
          required={props.required || null}
        >
          <option value='' disabled>Select one option</option>

          {selectOptionsList}
        </select>
    </fieldset>
}

export default selectInput;