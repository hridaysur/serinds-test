import React from 'react';
import './app.scss';
import Form from './components/Form/form';
import Movie from './components/Movie/movie';
import LoadingSpinner from './components/LoadingSpinner/LoadingSpinner'
import OMDBService from './service/omdbAPI.service';
import Popup from './components/popup/popup.js';

class App extends React.Component {
  
  constructor() {
      super();
      this.state = {
        formData: null,
        movies: [],
        isLoading: false,
        typeOptions: ['movie', 'series', 'episode'],
        showPopup : false,
        movieDetail: {}
      };
  }

  togglePopup() {  
    this.setState({  
         showPopup: !this.state.showPopup  
    });  
     }  
 
  detailsClick = (id) => {
    let movie = OMDBService.getMovieByID(id)
        .then(movie => {
          this.setState({
            showPopup:true, 
            movieDetail: OMDBService.getMovieDetailModel(movie.result) 
          })
        })
  }

  setFormData = (fData) => {
    this.setState({ isLoading: true });
    OMDBService.getMovies(fData.title,this.state.typeOptions[fData.type], fData.year)
      .then(movie => {
        if(movie.success) {
          this.setState({
            movies: OMDBService.getMovieModel(movie.result),
            isLoading:false });
        } else {
          this.setState({ isLoading: false });
          alert(movie.status);
        }
      });
  }

  render() {
      //define movies field that loop through all the movies data
      let movies = null;
          
      movies = (
        this.state.movies.map((movie) => {
          return  < Movie
           key={movie.key}
           id={movie.imdbID}
           title={movie.title}
           image={movie.image}
           year={movie.year}
           type={movie.type}
           handleOnDetailsClick={this.detailsClick}
          />
        })
      )

       return (
          <div className="App">
            <div className="jumbotron">
              <h1 className="display-4">OMDb API</h1>
              <Form typeOptions = {this.state.typeOptions} handelOnSubmit={this.setFormData}/>
            </div>

              {this.state.isLoading ? <LoadingSpinner /> :  
              <div className="container">
                <div className="row movieContainer">
                  {movies}
                </div>
              </div>
              }

            { this.state.showPopup ?  
               <Popup 
                title={this.state.movieDetail.title}
                image={this.state.movieDetail.image}
                released={this.state.movieDetail.released}
                type={this.state.movieDetail.type}
                genre={this.state.movieDetail.genre}
                ratings={this.state.movieDetail.ratings}
                closePopup={this.togglePopup.bind(this)}  
                />  
              : null  
            }  
          </div>
      );
    }
  }

export default App;
